<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultsRepository")
 * @ORM\Table(
 *     name="results",
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_word_by_adapter",
 *            columns={"word", "adapter"})
 *    }
 * )
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Results
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $word;

    /**
     * @ORM\Column(type="smallint")
     */
    private $popularity;

    /**
     * @ORM\Column(type="bigint")
     */
    private $rocks;

    /**
     * @ORM\Column(type="bigint")
     */
    private $sucks;

    /**
     * @ORM\Column(type="bigint")
     */
    private $totalCount;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $adapter;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Results
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @param mixed $word
     * @return Results
     */
    public function setWord($word)
    {
        $this->word = $word;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * @param mixed $popularity
     * @return Results
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRocks()
    {
        return $this->rocks;
    }

    /**
     * @param mixed $rocks
     * @return Results
     */
    public function setRocks($rocks)
    {
        $this->rocks = $rocks;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSucks()
    {
        return $this->sucks;
    }

    /**
     * @param mixed $sucks
     * @return Results
     */
    public function setSucks($sucks)
    {
        $this->sucks = $sucks;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param mixed $totalCount
     * @return Results
     */
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @param mixed $adapter
     * @return Results
     */
    public function setAdapter($adapter)
    {
        $this->adapter = $adapter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Results
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return Results
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
