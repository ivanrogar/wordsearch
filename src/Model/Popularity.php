<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\Results;
use App\Model\Search\SearchAdapterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Exception;

/***
 * Class Popularity
 * @package App\Model
 */
class Popularity
{

    private $container;

    /**
     * @var SearchAdapterInterface|null
     */
    private $currentAdapter;

    /**
     * Popularity constructor.
     * @param ContainerInterface $container
     */
    public function __construct(
        ContainerInterface $container
    ) {
        $this->container = $container;
    }

    /**
     * Get word result using a specific adapter, calculate and save to database
     *
     * @param string $word
     * @param SearchAdapterInterface $adapter
     * @return Results|null
     */
    public function fetch(string $word, SearchAdapterInterface $adapter) : ?Results
    {
        $this->currentAdapter = $adapter;

        $word = trim($word);

        $result = $adapter->search($word);

        // todo: check and handle response code and errors, currently we're blind believers!

        $rocks = $result->getRocks();
        $sucks = $result->getSucks();
        $total = $rocks + $sucks;

        ($total && $rocks)
            ? $popularity = round(($rocks / $total) * 10)
            : $popularity = 0;

        $wordEntry = $this->getWordEntry($word);
        $wordEntry
            ->setPopularity($popularity)
            ->setRocks($rocks)
            ->setSucks($sucks)
            ->setTotalCount($total);

        return $this->saveWordEntry($wordEntry);
    }

    /**
     * @param string $word
     * @return Results
     */
    private function getWordEntry(string $word) : Results
    {
        $ema = $this->container->get('doctrine')->getManager();

        $wordEntry = $ema
            ->getRepository('App:Results')
            ->getWordByAdapter($word, $this->currentAdapter->getName());

        if (!$wordEntry) {
            $wordEntry = new Results();
            $wordEntry
                ->setAdapter($this->currentAdapter->getName())
                ->setWord($word);
        }

        return $wordEntry;
    }

    /**
     * @param Results $results
     * @return Results
     */
    private function saveWordEntry(Results $results) : Results
    {
        try {
            $ema = $this->container->get('doctrine')->getManager();
            $ema->persist($results);
            $ema->flush();
        } catch (Exception $e) {
            // todo: log and/or handle db errors
        }

        return $results;
    }
}
