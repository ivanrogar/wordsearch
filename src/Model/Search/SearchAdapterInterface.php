<?php

namespace App\Model\Search;

use \Exception;

/**
 * Interface SearchAdapterInterface
 * @package App\Model\Search
 */
interface SearchAdapterInterface
{

    /**
     * @param string $query
     * @return SearchResult
     */
    public function search(string $query) : SearchResult;

    /**
     * @return mixed
     */
    public function getData();

    /**
     * @param string $path
     * @return mixed
     */
    public function getDataPath(string $path);

    /**
     * @return \Exception|null
     */
    public function getException() : ?Exception;

    /**
     * @return int|null
     */
    public function getResponseStatusCode() : ?int;

    /**
     * @return string
     */
    public function getName() : string;
}
