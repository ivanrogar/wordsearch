<?php

namespace App\Model\Search;

/**
 * Convenient encapsulation for a single search result
 *
 * Class SearchResult
 * @package App\Model\Search
 */
class SearchResult
{

    private $sucks = null;

    private $rocks = null;

    /**
     * @return mixed
     */
    public function getSucks()
    {
        return $this->sucks;
    }

    /**
     * @param mixed $sucks
     * @return SearchResult
     */
    public function setSucks($sucks)
    {
        // don't allow modification
        if ($this->sucks === null) {
            $this->sucks = $sucks;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRocks()
    {
        return $this->rocks;
    }

    /**
     * @param mixed $rocks
     * @return SearchResult
     */
    public function setRocks($rocks)
    {
        // don't allow modification
        if ($this->rocks === null) {
            $this->rocks = $rocks;
        }

        return $this;
    }
}
