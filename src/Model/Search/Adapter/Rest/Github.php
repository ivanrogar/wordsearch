<?php

declare(strict_types=1);

namespace App\Model\Search\Adapter\Rest;

use App\Model\Search\AbstractSearchAdapter;
use App\Model\Search\SearchResult;

/**
 * Class Github
 * @package App\Model\Search\Adapter\Rest
 */
class Github extends AbstractSearchAdapter
{

    /**
     * @inheritDoc
     */
    public function search(string $query) : SearchResult
    {
        // get rocks
        $this
            ->doRequest('GET', 'https://api.github.com/search/issues?q=' . urlencode($query . ' rocks'));

        $rocks = (int) $this->getDataPath('total_count');

        // get sucks
        $this
            ->doRequest('GET', 'https://api.github.com/search/issues?q=' . urlencode($query . ' sucks'));

        $sucks = (int) $this->getDataPath('total_count');

        $result = new SearchResult();
        $result
            ->setRocks($rocks)
            ->setSucks($sucks);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'github';
    }
}
