<?php

declare(strict_types=1);

namespace App\Model\Search\Adapter\GraphQL;

use App\Model\Search\AbstractSearchAdapter;
use App\Model\Search\SearchResult;

/**
 * Class Github
 * @package App\Model\Search\Adapter\GraphQL
 */
class Github extends AbstractSearchAdapter
{

    /**
     * @inheritDoc
     */
    public function search(string $query) : SearchResult
    {
        // todo

        return new SearchResult();
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'github_graphql';
    }
}
