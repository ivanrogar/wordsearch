<?php

declare(strict_types=1);

namespace App\Model\Search;

use App\Model\ArrayTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use \Exception;

/**
 * Class AbstractSearchAdapter
 * @package App\Model\Search
 */
abstract class AbstractSearchAdapter implements SearchAdapterInterface
{

    use ArrayTrait;

    /**
     * @var Client|null
     */
    private $client;

    private $exception;

    private $responseStatusCode;

    private $responsePayload;

    /**
     * @return Client
     */
    protected function getClient() : Client
    {
        if (!$this->client) {
            $this->client = new Client();
        }

        return $this->client;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return $this
     */
    protected function doRequest(string $method, string $url, array $options = []) : AbstractSearchAdapter
    {
        $this->exception = null;
        $this->responseStatusCode = null;
        $this->responsePayload = null;

        try {
            $response = $this
                ->getClient()
                ->request($method, $url, $options);

            $this->responseStatusCode = $response->getStatusCode();

            if ($this->responseStatusCode == 200) {
                (!$this->isJsonResponse($response))
                    ? $this->responsePayload = $response->getBody()->getContents()
                    : $this->responsePayload = json_decode($response->getBody()->getContents(), true);
            }
        } catch (BadResponseException $e) {
            ($e->hasResponse())
                ? $this->responseStatusCode = $e->getResponse()->getStatusCode()
                : $this->responseStatusCode = -1;   // no response code

            $this->exception = $e;
        } catch (GuzzleException $e) {
            $this->exception = $e;
        }

        return $this;
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    protected function isJsonResponse(ResponseInterface $response) : bool
    {
        $contentTypes = $response->getHeader('content-type');
        if (!is_array($contentTypes)) {
            $contentTypes = [$contentTypes];
        }

        foreach ($contentTypes as $contentType) {
            if (stripos($contentType, 'application/json;') !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getData()
    {
        return $this->responsePayload;
    }

    /**
     * @inheritDoc
     */
    public function getDataPath(string $path)
    {
        return $this->getArrayPath($this->responsePayload, $path);
    }

    /**
     * @inheritDoc
     */
    public function getException() : ?Exception
    {
        return $this->exception;
    }

    /**
     * @inheritDoc
     */
    public function getResponseStatusCode() : ?int
    {
        return $this->responseStatusCode;
    }
}
