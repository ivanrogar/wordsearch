<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Trait ArrayTrait
 * @package App\Model
 */
trait ArrayTrait
{

    /**
     * @param $data
     * @param string $path
     * @return mixed
     */
    public function getArrayPath($data, string $path)
    {
        if (!is_array($data)) {
            return null;
        }

        $searchKeys = explode('/', $path);
        $start = $data;

        foreach ($searchKeys as $searchKey) {
            $searchKey = trim($searchKey);
            if (!isset($start[$searchKey])) {
                return null;
            }

            $start = $start[$searchKey];
        }

        return $start;
    }
}
