<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="home_page")
     * @return Response
     */
    public function indexAction()
    {
        return $this
            ->render(
                'index.html.twig'
            );
    }
}
