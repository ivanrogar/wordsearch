<?php

namespace App\Controller;

use App\Model\Search\Adapter\Rest\Github;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SearchController
 * @package App\Controller
 * @Route("/search")
 */
class SearchController extends Controller
{

    /**
     * @param string $word
     * @Route("/{word}", name="word_search", methods={"POST"})
     * @return JsonResponse
     */
    public function indexAction($word)
    {
        $ema = $this->get('doctrine')->getManager();

        // find in database
        $entry = $ema->getRepository('App:Results')->getWordByAdapter($word, 'github');
        if (!$entry) {
            // ask github
            $popularity = $this->get('some_word_popularity');
            $adapter = new Github();
            $entry = $popularity->fetch($word, $adapter);
        }

        return new JsonResponse(
            [
                'rocks' => $entry->getRocks(),
                'sucks' => $entry->getSucks(),
                'popularity' => $entry->getPopularity(),
                'total' => $entry->getTotalCount(),
            ]
        );
    }
}
