<?php

namespace App\Command;

use App\Model\Search\Adapter\Rest\Github;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PopularityCommand
 * @package App\Command
 */
class PopularityCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('app:word-popularity:search')
            ->setDescription('Find word/term popularity')
            ->addOption('word', null, InputOption::VALUE_REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output) : ?int
    {

        if (!trim($input->getOption('word'))) {
            $output->writeln('parameter --word missing');
            return -1;
        }

        $wordPopularity = $this
            ->getContainer()
            ->get('some_word_popularity');

        $adapter = new Github();

        $entry = $wordPopularity
            ->fetch($input->getOption('word'), $adapter);

        $output->writeln('Popularity: ' . $entry->getPopularity());
        $output->writeln('Rocks: ' . $entry->getRocks());
        $output->writeln('Sucks: ' . $entry->getSucks());
        $output->writeln('Total: ' . $entry->getTotalCount());

        return 0;
    }
}
