<?php

namespace App\Repository;

use App\Entity\Results;
use Doctrine\ORM\EntityRepository;
use \Exception;

/**
 * Class ResultsRepository
 * @package App\Repository
 */
class ResultsRepository extends EntityRepository
{

    /**
     * @param string $word
     * @param string $adapterName
     * @return Results|null
     */
    public function getWordByAdapter(string $word, string $adapterName) : ?Results
    {
        $query = $this->createQueryBuilder('q');

        try {
            return $query
                ->andWhere('q.adapter = :adapterName')
                ->setParameter('adapterName', $adapterName)
                ->andWhere('q.word LIKE :word')
                ->setParameter('word', trim($word))
                ->getQuery()
                ->getSingleResult();
        } catch (Exception $e) {
            return null;
        }
    }
}
