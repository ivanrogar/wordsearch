import React, {Component} from "react";

class SearchInput extends Component {

    constructor(props) {
        super(props);

        this.state = {
            result: {"rocks": "-", "sucks": "-", "total": "-", "popularity": "-"},
            search: '',
            searchDisabled: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleChange(e) {
        this.setState({search: e.target.value});
    }

    handleSearch() {
        if (this.state.search.trim() !== '') {
            this.setState({
                searchDisabled: true,
                result: {"rocks": "-", "sucks": "-", "total": "-", "popularity": "-"},
                search: ''
            });

            fetch('/search/' + encodeURIComponent(this.state.search.trim()), {
                credentials: "same-origin",
                method: 'POST'
            })
                .then(response => response.json())
                .then((data => {
                        this.setState({result: data});
                        this.setState({searchDisabled: false});
                    })
                )
                .catch((error) => {
                    this.setState({searchDisabled: false});
                });
        }
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 col-12">
                    <br/>
                    <div className="row">

                        <div className="col-md-9 col-lg-9 col-sm-9 col-xs-12 col-12">
                            <input type="text" value={this.state.search} onChange={this.handleChange}
                                   placeholder="Enter word" style={{"width": "100%"}}/>
                        </div>
                        <div className="col-md-3 col-lg-3 col-sm-3 col-xs-12 col-12">
                            <button type={'button'} className={'btn btn-success'} onClick={this.handleSearch}
                                    disabled={this.state.searchDisabled}>Search
                            </button>
                        </div>
                    </div>

                    <br/>
                    <div className="row">
                        <div className="col-md-3 col-lg-3 col-sm-12 col-xs-12 col-12">
                            <h3>Rocks: {this.state.result.rocks}</h3>
                        </div>
                        <div className="col-md-3 col-lg-3 col-sm-12 col-xs-12 col-12">
                            <h3>Sucks: {this.state.result.sucks}</h3>
                        </div>
                        <div className="col-md-3 col-lg-3 col-sm-12 col-xs-12 col-12">
                            <h3>Popularity: {this.state.result.popularity}</h3>
                        </div>
                        <div className="col-md-3 col-lg-3 col-sm-12 col-xs-12 col-12">
                            <h3>Total: {this.state.result.total}</h3>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SearchInput;